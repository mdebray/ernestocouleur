
**Un version web est disponible à l'adresse suivante :** [https://www.eleves.ens.fr/home/mdebray/ernestocouleur](https://www.eleves.ens.fr/home/mdebray/ernestocouleur)

# Ernestocouleurs v3

Le script `ernestocouleur.py` a pour but de créer les arcs-en-ciel des chefs fanfare (et d'autres personnes si elles aiment la couleur).
Il lit le fichier texte `entree.txt` et rend du code html coloré dans `sortie.txt`.
Si vous voulez contribuer, vous pouvez.

## Utilisation

Il faut `python3` sur son ordinateur

1. Mettez votre texte dans `entree.txt`
2. Lancez le script
3. Trois modes sont disponibles:
   - Mode 1: La couleur chage pour chaque caractère
   - Mode 2: Chaque ligne est de couleur uniforme (gradient de couleur vertical)
   - Mode 3: Comme le mode 1 mais l'arc-en-ciel est réinitialisé à chaque ligne
4. Choisissez le nombre d'arc-en-ciels
5. Le programme vous demande si vous voulez écraser le fichier `sortie.txt`

Pour thunderbird, il suffit de cliquer sur `Insérer > HTML...` puis de rentrer le contenu de `sortie.txt` dans la boîte de dialogue.

## print\_colors

Le fichier `print_colors.py` permet de créer un page HTML affichant les couleurs en coordonnées (hue, saturation).
Il faut a priori piper la sortie standard vers le fichier HTML souhaité

## License

Ce code est dans le domaine public.
Pour les capitalistes vénaux, vous pouvez toujours essayer de le vendre...
Si vous contribuez vous acceptez que votre contribution soit versée au domaine public.

## TODO

Mettre les couleurs des arc-en-ciel de `ernestophone.cof.ens.fr` qui sont un peu mieux.
