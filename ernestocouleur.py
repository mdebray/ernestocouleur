from os.path import exists

def sat(hue):
    """
    Donne la saturation requise pour la valeur de teinte donnée
    """
    if hue <= 35:
        return 100
    if hue <= 55:
        return int(100 - (hue - 35))
    if hue <= 70:
        return 80
    if hue <= 80:
        return 150 - hue
    if hue <= 150:
        return 70
    if hue <= 180:
        return 60 #int(70 - (hue - 150)*3/2)
    if hue <= 200:
        return int(60 + (hue-180)*2) #int(45 + (hue - 180)/4*11)
    return 100



if not exists('entree.txt'):
    print("Veuillez mettre le texte dans le fichier 'entree.txt'")
else:
    texte = ""
    with open('entree.txt') as f:
        texte = f.read()
    mode = int(input("1. Mode basique (le programme ignore les nouvelles lignes\n2. Mode Ligne par ligne (l'arc en ciel est réalisé sur les lignes plutôt que sur les carctères\n3. Mode recommencer pour chaque ligne\nQuel mode ? >"))

    n = float(input("Nombre d'arcs en ciel >"))
    if mode == 1:
        unit_coloree = [list(texte)]
    elif mode == 2:
        unit_coloree = [[i + "\n" for i in texte.split("\n")]]
    elif mode == 3:
        unit_coloree = [i + "\n" for i in texte.split("\n")]


    resultat = ""

    for t in unit_coloree:
        pas = 360/len(t)*n
        for i in range(len(t)):
            resultat += f'<span style="color:hsl({int(i*pas)}, {sat(int(i*pas))}%, 50%)">{t[i]}</span>'
    resultat = resultat.replace("\n", "</br>\n")

    if not(exists('sortie.txt') and input("/!\ le fichier 'sortie.txt' va être écrasé. Continuer ? (o/n)\n>") == 'n'):
        with open("sortie.txt", 'w') as f:
            f.write(resultat)
